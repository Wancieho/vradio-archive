/*
 * Project: vRadio
 * Description:
 * Author: Vaughan Webber - www.vaughanderful.co.za
 * License:
 * Version: 1.0.0
 * Dependancy: jquery-1.10.2 (jquery.com)
 * Date: 26/09/2013
 */

;
(function($) {
	var pluginName = "vRadio", defaults = {
	};

	function vRadio(element, options) {
		this.element = $(element);
		this.wrapper;
		this.options = $.extend({}, defaults, options);

		this.__constructor();
	}

	vRadio.prototype = {
		__constructor: function() {
			this.create();
			this.events();
		},
		create: function() {
			this.element.wrap('<div class="vRadio clearfix"></div>');

			var wrapper = this.wrapper = this.element.parent();

			wrapper.append('<a href="#" class="vDot"></a>').find('.vDot').append('<div>');

			if (this.element.prop('checked'))
				this.element.siblings('a').addClass('selected');
		},
		events: function() {
			this.wrapper.find('a').on('click', function(e) {
				e.preventDefault();

				var name = $(this).siblings('input').attr('name');

				$('.vRadio input[name="' + name + '"]').removeProp('checked').siblings('a').removeClass('selected');

				$(this).siblings('input').prop('checked', true).siblings('a').addClass('selected');
			});
		}
	};

	$.fn.vRadio = function(options) {
		return this.each(function() {
			if (!$.data(this, "plugin_" + pluginName))
				$.data(this, "plugin_" + pluginName, new vRadio(this, options));
		});
	};
})(jQuery);